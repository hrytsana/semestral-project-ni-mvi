# Semestral project _(by Anastasiia Hrytsyna)_

## Content of the folder

-  **report_first.pdf** - submission of first milestone
-  **program.ipynb** - assignment code that contains saved outputs and plots
-  **report.pdf** - final report

## Quick tutorial
Import **program.ipynb** to the Kaggle platform. Then add input data from this [link](https://www.kaggle.com/datasets/clmentbisaillon/fake-and-real-news-dataset) and run the whole project. 
